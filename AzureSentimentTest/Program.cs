﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;
using Microsoft.Rest;
using SentimentAnalysisPaper.Data.Enums;
using SentimentAnalysisPaper.Data.Loaders;

namespace AzureSentimentTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = CreateAzureClient();

            var dataLoaderFactory = new AirlinesDataLoaderFactory();
            var airlinesDataLoader = dataLoaderFactory.GetLoader();
            var airlinesData = airlinesDataLoader.Load().ToList();

            int correctAnswersNumber = 0;
            var recordsDictionary = airlinesData.Select((record, index) => new { index, record })
                .ToDictionary(x => x.index.ToString(), x => x.record.Sentiment);

            var multilanguageInputChunks = PrepareInputChunks(airlinesData, 1000);
            foreach (var groupedChunks in multilanguageInputChunks)
            {
                var batch = new MultiLanguageBatchInput(groupedChunks.ToList());
                
                var sentimentBatchResult = client.Sentiment(batch);

                var correctBatchAnswers = sentimentBatchResult.Documents.Where(x =>
                {
                    if (!x.Score.HasValue)
                        return false;

                    var score = MapScoreToSentiment(x.Score.Value);
                    return score == recordsDictionary[x.Id];
                }).Count();

                correctAnswersNumber += correctBatchAnswers;
            }

            //calculate score
            var accuracy = correctAnswersNumber / recordsDictionary.Count;
            Console.WriteLine("====================================== Prediction complete ================================");
            Console.WriteLine($"Model accuracy: {accuracy}");

            Console.OutputEncoding = System.Text.Encoding.UTF8;
        }

        private static Sentiment MapScoreToSentiment(double scoreValue)
        {
            if (scoreValue < 0.5)
                return Sentiment.Negative;

            if (Math.Abs(scoreValue - 0.5) < 0.05)
                return Sentiment.Neutral;

            return Sentiment.Positive;
        }

        private static ITextAnalyticsAPI CreateAzureClient()
        {
// Create a client.
            ITextAnalyticsAPI client = new TextAnalyticsAPI();
            client.AzureRegion = AzureRegions.Westeurope;
            client.SubscriptionKey = "6427c7faf7b1402c8838e7c4b4a2ce02";
            return client;
        }

        private static IEnumerable<IEnumerable<MultiLanguageInput>> PrepareInputChunks(List<AirlineSentimentRecord> airlinesData, int chunkSize)
        {
            //prepare data
            return airlinesData.Select((record, index) => new MultiLanguageInput()
            {
                Id = index.ToString(),
                Language = "en",
                Text = record.Text

            }).ChunkBy(chunkSize);
        }
    }
}
