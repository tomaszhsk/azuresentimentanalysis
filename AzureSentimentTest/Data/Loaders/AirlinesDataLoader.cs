﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;

namespace SentimentAnalysisPaper.Data.Loaders
{
    public class AirlinesDataLoader
    {
        private const string DataFilePath = "C:\\Praca\\Projekty\\SentimentAnalysisPaper\\SentimentAnalysisPaper\\Data\\twitter-airlines-sentiment.csv";
        private readonly AirlineSentimentRecordClassMap _classMap;

        public AirlinesDataLoader(AirlineSentimentRecordClassMap classMap)
        {
            _classMap = classMap;
        }

        public IEnumerable<AirlineSentimentRecord> Load()
        {
            using (var reader = new CsvReader(new StreamReader(DataFilePath)))
            {
                reader.Configuration.RegisterClassMap(_classMap);
                var records = reader.GetRecords<AirlineSentimentRecord>();
                return records.ToList();
            }
        }
    }
}