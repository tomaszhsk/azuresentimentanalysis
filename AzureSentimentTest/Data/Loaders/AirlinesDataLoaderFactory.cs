﻿using SentimentAnalysisPaper.Processing.Preprocessors;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Data.Loaders
{
    public class AirlinesDataLoaderFactory
    {
        public AirlinesDataLoader GetLoader()
        {
            var textPreprocessors = new ITextPreprocessor[] {new HashtagAndMentionTextPreprocessor()};
            var sentimentMapper = new SentimentToEnumMapper();

            var classMap = new AirlineSentimentRecordClassMap(textPreprocessors, sentimentMapper);
            return new AirlinesDataLoader(classMap);
        }
    }
}