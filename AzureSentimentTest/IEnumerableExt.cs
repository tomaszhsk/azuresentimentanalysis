﻿using System.Collections.Generic;
using System.Linq;

namespace AzureSentimentTest
{
    public static class EnumerableExt
    {
        public static IEnumerable<IEnumerable<TSource>> ChunkBy<TSource>
            (this IEnumerable<TSource> source, int chunkSize)
        {
            while (source.Any())                     // while there are elements left
            {   // still something to chunk:
                yield return source.Take(chunkSize); // return a chunk of chunkSize
                source = source.Skip(chunkSize);     // skip the returned chunk
            }
        }
    }
}