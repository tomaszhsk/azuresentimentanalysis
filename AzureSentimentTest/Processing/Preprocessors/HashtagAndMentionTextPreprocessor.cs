﻿using System.Text.RegularExpressions;
using SentimentAnalysisPaper.Processing.TextPreprocessors;

namespace SentimentAnalysisPaper.Processing.Preprocessors
{
    public class HashtagAndMentionTextPreprocessor : ITextPreprocessor
    {
        private readonly string _hashTagAndMentionRegex = "[\\s]*([@#][\\w_-]+)";

        public string Process(string text)
        {
            return Regex.Replace(text, _hashTagAndMentionRegex, string.Empty);
        }
    }
}
